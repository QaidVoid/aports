# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=15.3.0
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/ocrmypdf/OCRmyPDF"
# s390x, armhf, x86, ppc64le: tesseract-ocr
arch="noarch !s390x !armhf !x86 !ppc64le"
license="MIT"
depends="
	ghostscript
	jbig2enc
	leptonica
	pngquant
	py3-deprecation
	py3-img2pdf
	py3-packaging
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-pluggy
	py3-reportlab
	py3-rich
	python3
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="
	py3-gpep517
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-hypothesis
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	tesseract-ocr-data-eng
	tesseract-ocr-data-osd
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH=src \
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/ocrmypdf*.whl
}

sha512sums="
7befe3e378788c3ced874f68d4780ddbeb4cf260e31f6f962cf2a2d4060b766301bd1f1838ca29af530fe8d3e824a2bc265322a3ea204a3bf74c62c922757e54  ocrmypdf-15.3.0.tar.gz
"
