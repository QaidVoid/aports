# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=27.3.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
c1b7064ac8843496c76f0ea3dee39d56cb6e27c878a6e3160c1bcad60718658828eb5561657fe4a94e2c3fb04d55120a88a2964bf43d323f5cee885e5bfb6420  super-ttc-iosevka-27.3.1.zip
f2f02f60776c766fabc580be5102384bd172b54137c3f9a3475c8c208b807fb805d3f77a96a2c6ec5ed7d632a0debda9ff4b06c678da052f59ebbceeb3ba80e2  super-ttc-iosevka-aile-27.3.1.zip
b3cd70acc2d90ed06ec7f4b0e97cbd82696493171b361575cfc2a67f61e9927d49bd0a30b30877ebc4cc1e28f23a1af609f7d3f05a0dea9d4477d44f59ba6986  super-ttc-iosevka-slab-27.3.1.zip
81523ff567a0511136742456003797b39d43c1e7fecdb5bde5764e8b94f54a16bb54036faa7f41113a705f127104835f07b9b73ec86d62c9db5f922793bb1130  super-ttc-iosevka-curly-27.3.1.zip
a37f1a31cad08d94a5fe0c02811844c7b5e755048ec7f557254c938dbde6e1af0fbf242c3babbfabb56225b840ee577000f6d120fc6a686b95d3fea844249f3a  super-ttc-iosevka-curly-slab-27.3.1.zip
"
