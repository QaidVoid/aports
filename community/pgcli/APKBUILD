# Contributor: Thomas Boerger <thomas@webhippie.de>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=pgcli
pkgver=3.5.0
pkgrel=3
pkgdesc="Postgres CLI with autocompletion and syntax highlighting"
options="!check" # Tests fail
url="https://www.pgcli.com"
arch="noarch"
license="BSD-3-Clause"
depends="python3
	py3-cli_helpers>1.0.0
	py3-click
	py3-configobj
	py3-humanize
	py3-gpep517
	py3-pendulum
	py3-pgspecial
	py3-prompt_toolkit>2.0.0
	py3-psycopg-c
	py3-pygments
	py3-setproctitle
	py3-setuptools
	py3-sqlparse<0.5.0
	py3-wcwidth
	py3-wheel
	"
checkdepends="pytest py3-mock"
makedepends="python3-dev"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/p/pgcli/pgcli-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d72cec375fd85aa83e90a3b88877b32df2143c47968c9531417dddb0e7de5463827668a5af78e4dddce3ddc1cf9baa97ada7271bd3844f7dea047adc7fca076f  pgcli-3.5.0.tar.gz
"
