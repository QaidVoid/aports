# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=buildah
pkgver=1.32.1
pkgrel=0
pkgdesc="tool that facilitates building OCI container images"
url="https://github.com/containers/buildah"
license="Apache-2.0"
arch="all"
depends="oci-runtime shadow-subids slirp4netns containers-common"
makedepends="go go-md2man lvm2-dev gpgme-dev libseccomp-dev btrfs-progs-dev bash"
subpackages="$pkgname-doc"
options="!check" # tests require root privileges
source="https://github.com/containers/buildah/archive/v$pkgver/buildah-$pkgver.tar.gz"

# secfixes:
#   1.28.0-r0:
#     - CVE-2022-2990
#   1.21.3-r0:
#     - CVE-2021-3602
#   1.19.4-r0:
#     - CVE-2021-20206
#   1.14.4-r0:
#     - CVE-2020-10696

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	# https://github.com/mattn/go-sqlite3/issues/1164
	export CGO_CFLAGS="$CFLAGS -D_LARGEFILE64_SOURCE"

	GIT_COMMIT="$pkgver" make
}

package() {
	GIT_COMMIT="$pkgver" make install PREFIX=/usr DESTDIR="$pkgdir"
}

sha512sums="
e5c59f158b786809c89465361a662211bbef6bb4c27c79490604923d924c2030398a0878b122b246b02ae246bd977b7ccffdc6de4f06e80f096c2921c34bf04a  buildah-1.32.1.tar.gz
"
