# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=fclones
pkgver=0.33.1
pkgrel=0
pkgdesc="Efficient duplicate file finder"
url="https://github.com/pkolaczk/fclones"
license="MIT"
arch="all !s390x" # incompatible with nix crate
arch="$arch !armhf !armv7 !x86 !ppc64le" # tests fail
makedepends="cargo eudev-dev cargo-auditable"
source="https://github.com/pkolaczk/fclones/archive/v$pkgver/fclones-$pkgver.tar.gz
	musl-fix.patch
	"
options="net"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
673da399fabf86fdb84e14fe64fd1b29d2b41ca3632135d6b6f74c385869065bcd8c05e14fbf43fb989ca5ac0d766294fa313815c979bd7ffe029515f1b48056  fclones-0.33.1.tar.gz
8e8a3ba0d2e66f7fd909e1992a8f90f6ac9307b456a3825beb23337ff5a48bc30809b694ccfce3c7208ee48fa6b93c7185841d628815ca8b8bb18f14522a65ff  musl-fix.patch
"
